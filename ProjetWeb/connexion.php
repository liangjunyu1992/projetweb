<?php 	

require_once("param.inc.php");

try
{
	$bdd = new PDO($dbhost,$dbuser,$dbpassword);
}
catch (Exception $e)
{
	die('Erreur : ' . $e->getMessage());
}

//Nettoyage des données
$_POST['login']=htmlentities($_POST['login'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
$_POST['mdp']=htmlentities($_POST['mdp'], ENT_QUOTES | ENT_IGNORE, "UTF-8");

// Hachage du mot de passe
$pass_hache = sha1($_POST['mdp']);

// Vérification des identifiants
$req = $bdd->prepare('SELECT id_user, login_user, type_user FROM user WHERE login_user = :login AND mdp_user = :mdp');
$req->execute(array(
    'login' => $_POST['login'],
    'mdp' => $pass_hache))or die(print_r($req->errorInfo()));

$resultat = $req->fetch();

if (!$resultat)
{
    echo 'Mauvais identifiant ou mot de passe !';
}
else
{
	session_start();
	$_SESSION['id']=$resultat['id_user'];
    	$_SESSION['login'] = $resultat['login_user'];
    	$_SESSION['type'] = $resultat['type_user'];
    	if ($_SESSION['type']=='Cherc')
    	{
    		$_SESSION['message']='cherc';
    		header('Location: Chercheur.php');
    	}
    	else
    	{
    		$_SESSION['message']='admin';
    		header('Location: Admin.php');
	}
}

	
?>
