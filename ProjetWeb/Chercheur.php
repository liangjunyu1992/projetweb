<?php 	session_start(); 
		require_once("param.inc.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Chercheur</title>
		
	</head>
	<body>
		
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menuCon.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1>Liste des projets</h1></header>
			
			<?php
			
			if ($_SESSION['message']=='cherc')
			{
				echo '<span id="conChercheur">Connexion réussie en tant que Chercheur</span>';
				$_SESSION['message']='';
			}
			
			?>
			
			<div class="contenu">
			
			<?php
			
			if ($_SESSION['type']=='Cherc')
			{ ?>
				
			
			<div align="center" class="liste">
			
				<table>
					<tr>
						<th>Nom Projet</th>
						<th>Description Projet</th>
					</tr>
					
				<?php
				
					//connexion à la base de donnée

					try
					{
						$bdd = new PDO($dbhost,$dbuser,$dbpassword);
					}
					catch (Exception $e)
					{
						die('Erreur : ' . $e->getMessage());
					}
				
					//affichage des lignes du tableau avec nom et description du projet
				
					$reponse = $bdd->query('SELECT * FROM projet ORDER BY nom_pro ASC') or die(print_r($req->errorInfo()));
					while ($donnees = $reponse->fetch())
					{ ?>
						<tr>
							<td> <?php echo $donnees['nom_pro']; ?> </td>
							<td> <?php echo $donnees['desc_pro']; ?> </td>
							<td> 
							<form method="post" action="afficherDetailProjet.php">
								<button name="detailProjet" value="<?php echo $donnees['id_pro']; ?>">Afficher Détails</button>
							</form>
							</td>
							<td> 
							<?php // On vérifie su l'utilisateur n'a pas déjà rejoint le projet
							
							$req2 = $bdd ->prepare('SELECT * 
										FROM user_par_projet 
										WHERE id_projet_upp=:id_projet AND id_user_upp=:id_user');
							$req2->execute(array(
							    'id_projet' => $donnees['id_pro'],
							    'id_user' => $_SESSION['id'])) or die(print_r($req->errorInfo()));
							$resultat = $req2->fetch();
							
							if (!$resultat)
							{
							 ?>
							<form method="post" action="rejoindreProjet.php">
								<button name="idProRejoint" value="<?php echo $donnees['id_pro']; ?>">Rejoindre</button>
							</form>
						<?php	}
							$req2->closeCursor(); ?>
							</td>
						</tr>
				<?php   }
					$reponse->closeCursor();
				
				?>
					
				</table>	
			
			</div>
			
			<?php
			}
			else
			{ 
				echo 'Vous n\'avez pas les droits nécessaire pour créer de nouveau projet.<br> Veuillez vous connecter en tant que Chercheur.';
			}
			?>
						
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
		
	</body>
</html>
