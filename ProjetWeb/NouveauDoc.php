<?php session_start();	?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Nouvel Utilisateur</title>
		<script src="javascript/passwordGen.js"></script>
	</head>
	<body>
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menuCon.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1>Création d'un nouvel utilisateur</h1></header>
			
			<div class="contenu">
				<div id="nouveauDoc">
				
				<?php
				
				if ($_SESSION['type']=='Cherc')
				{
				?>
				
				<form method="POST" action="ajouterDoc.php" name="formu" enctype="multipart/form-data">
						<label for="document">Emplacement du document</label>
						<input type="file" id="document" name="document" required>
						<br>
						<label for="descDoc">Description du document</label>
						<textarea id="descDoc" name="descDoc" row="10" cols="50" required></textarea>
						<br>
						<button name="envoi" value="ok">Ajouter le document</button>
				</form>
				
				<?php 
				}
				else 
				{
					echo 'Vous n\'avez pas les droits nécessaire pour créer de nouveau utilisateur.<br> Veuillez vous connecter en tant qu\'Administrateur.';
				}
				?>
					
				</div>
					
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
		
	</body>
</html>
