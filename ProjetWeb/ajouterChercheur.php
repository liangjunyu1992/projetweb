<?php 

session_start(); 
require_once("param.inc.php");

//connexion à la base de donnée

try
{
	$bdd = new PDO($dbhost,$dbuser,$dbpassword);
}
catch (Exception $e)
{
	die('Erreur : ' . $e->getMessage());
}

//Nettoyage des données
$_POST['login']=htmlentities($_POST['login'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
$_POST['result']=htmlentities($_POST['result'], ENT_QUOTES | ENT_IGNORE, "UTF-8");
$_POST['type']=htmlentities($_POST['type'], ENT_QUOTES | ENT_IGNORE, "UTF-8");

//Vérification de la validité des informations

//Vérification de l'inexistence du login

$req = $bdd->prepare('SELECT login_user FROM user WHERE login_user=:login');
$req->execute(array(
	'login'=>$_POST['login'])) or die(print_r($req->errorInfo()));
$resultat=$req->fetch();	
if (!$resultat) //création de l'utilisateur
{
	//Hachage du mot de passe
	$pass_hache= sha1($_POST['result']);

	//ajout de l'utilisateur à la base de donnée

	$req = $bdd ->prepare('INSERT INTO user(login_user,mdp_user,type_user) VALUES(:login,:mdp,:type)');
	$req->execute(array(
	    'login' => $_POST['login'],
	    'mdp' => $pass_hache,
	    'type' => $_POST['type'])) or die(print_r($req->errorInfo()));
	    
	$req->closeCursor();
	
	$_SESSION['userExist']='';
	$_SESSION['message']='creer';
}

//redirection vers la page NouveauChercheur.php
header('Location: NouveauChercheur.php');
    
?>
