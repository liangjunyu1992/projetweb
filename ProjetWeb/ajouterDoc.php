<?php

session_start();
require_once("param.inc.php");

// Testons si le fichier a bien été envoyé et s'il n'y a pas d'erreur
if (isset($_FILES['document']) AND $_FILES['document']['error'] == 0)
{
 // Testons si le fichier n'est pas trop gros 5Mo
       if ($_FILES['document']['size'] <= 5242880)
        {
 		// Testons si l'extension est autorisée
                $infosfichier = pathinfo($_FILES['document']['name']);
                $extension_upload = $infosfichier['extension'];
                $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png','pdf','doc','docx','odt','xls','ppt','pptx');
                if (in_array($extension_upload, $extensions_autorisees))
                {
                	// On peut valider le fichier et le stocker définitivement
                	$random =substr(md5(uniqid(rand(), true)),0,5); //on génère un id unique de 5 caractères pour pouvoir ajouter plusieurs fichier du même nom
                	$nomfichier = $infosfichier['filename'] .'.'. $random . '.' . $infosfichier['extension'];
                        move_uploaded_file($_FILES['document']['tmp_name'], 'uploads/' . $nomfichier);
                        
                        //protection contre les attaques
                        $_POST['descDoc']=htmlentities($_POST['descDoc'], ENT_QUOTES, "UTF-8");
                        
                        //On ajoute le doc à la base de donnée pour l'indexer
                        
                        //connexion à la base de donnée
			try
			{
				$bdd = new PDO($dbhost,$dbuser,$dbpassword);
			}
			catch (Exception $e)
			{
				die('Erreur : ' . $e->getMessage());
			}
			
			$req = $bdd ->prepare('INSERT INTO 
						document(nom_doc, desc_doc,id_projet_doc,id_user_doc, nom_stock_doc) 
						VALUES
						(:nom,:desc,:id_pro,:id_user, :nom_stock)');
			$req->execute(array(
				'nom'=>$infosfichier['basename'],
				'desc'=>$_POST['descDoc'],
				'id_pro'=>$_SESSION['idProjetCourant'],
				'id_user'=>$_SESSION['id'],
				'nom_stock'=>$nomfichier)) or die(print_r($req->errorInfo()));
			$req->closeCursor();
			
			$_SESSION['message']='ajouter';
			
			
                }
        }
}
if ($_SESSION['message']!='ajouter')
{
	$_SESSION['message']='erreur';
}
header('Location: DetailProjet.php');
?>
