<?php

session_start();
require_once("param.inc.php");

//connexion à la base de donnée

try
{
	$bdd = new PDO($dbhost,$dbuser,$dbpassword);
}
catch (Exception $e)
{
	die('Erreur : ' . $e->getMessage());
}

//On empêche l'administrateur de se supprimer lui-même.

if ($_SESSION['id'] !=$_POST['idSupprimmer'])
{
//On trouve et on supprimme d'utilisateur dans la base de donner utilisateur avec son id.
	$req=$bdd->prepare('DELETE FROM user WHERE id_user = :id_del');
	$req->execute(array(
		'id_del'=>$_POST['idSupprimmer'])) or die(print_r($req->errorInfo()));
	$req->closeCursor();

//On trouve et on supprimme les indications que l'utilisateur avait rejoint les projets dans la base user_par_projet
	$req=$bdd->prepare('DELETE FROM user_par_projet WHERE id_user_upp=:id_del');
	$req->execute(array(
		'id_del'=>$_POST['idSupprimmer'])) or die(print_r($req->errorInfo()));
	$req->closeCursor();

//On confirme la suppression de l'utilisateur en affichant un message.
	$_SESSION['message']='confirmesuppression';
}
else 
{
	$_SESSION['message']='DNDY';
}
//On recharge la page Admin.php

header('Location: Admin.php');

?>
