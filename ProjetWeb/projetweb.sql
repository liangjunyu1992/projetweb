-- phpMyAdmin SQL Dump
-- version 4.2.12
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 23 Novembre 2014 à 10:22
-- Version du serveur :  10.0.14-MariaDB-log
-- Version de PHP :  5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projetweb`
--

-- --------------------------------------------------------

--
-- Structure de la table `document`
--

CREATE TABLE IF NOT EXISTS `document` (
`id_doc` int(11) NOT NULL,
  `nom_doc` varchar(10) NOT NULL,
  `desc_doc` text NOT NULL,
  `id_projet_doc` int(11) NOT NULL,
  `id_user_doc` int(11) NOT NULL COMMENT 'Utilisateur ayant ajouté le document'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

CREATE TABLE IF NOT EXISTS `projet` (
`id_pro` int(11) NOT NULL,
  `nom_pro` varchar(10) NOT NULL,
  `desc_pro` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `projet`
--

INSERT INTO `projet` (`id_pro`, `nom_pro`, `desc_pro`) VALUES
(2, 'test2', 'tu pue le caca !');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id_user` int(11) NOT NULL,
  `login_user` varchar(20) NOT NULL,
  `mdp_user` varchar(255) NOT NULL,
  `type_user` char(5) NOT NULL DEFAULT 'Cherc'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id_user`, `login_user`, `mdp_user`, `type_user`) VALUES
(1, 'test', '987ff8ff7f86730485d99ca02ef8587eb23b13d6', 'Cherc'),
(2, 'test2', '1ca67bf6fe86fd3d83f78fb46e19b633f5bf2d60', 'Admin'),
(4, 'testons', '1f0a61cb8d800a79e4272aaeae7aeb17673da1b2', 'Cherc'),
(5, 'testonnour', '8e063012c1b68df6a5a6ae85fbc78e8b616f7d19', 'Cherc'),
(6, 'test3', 'b159b89dcfcaa428247f4c29b03a52eb4a67972d', 'Cherc'),
(7, '&lt;strong', '719267bb2d04b985b27a50f0503b7eadb2997b88', 'Cherc'),
(8, '&#039;test&#039;', '01ab1cf3bb0a38c22fa16bfedd1ed7bc8218e98e', 'Cherc'),
(9, 'test4', '6e53973764d0e763ffbe42de3bd97e3e6a8060ba', 'Cherc'),
(10, 'test6', '2cbade1bd94add1c34fe861ef6cca81e81f64cb0', 'Cherc'),
(11, 'admin', '5423c6228a436119506b091c953f6a787aa457be', 'Admin');

-- --------------------------------------------------------

--
-- Structure de la table `user_par_projet`
--

CREATE TABLE IF NOT EXISTS `user_par_projet` (
  `id_user_upp` int(10) NOT NULL,
  `id_projet_upp` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `document`
--
ALTER TABLE `document`
 ADD PRIMARY KEY (`id_doc`);

--
-- Index pour la table `projet`
--
ALTER TABLE `projet`
 ADD PRIMARY KEY (`id_pro`), ADD UNIQUE KEY `nom_pro` (`nom_pro`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id_user`);

--
-- Index pour la table `user_par_projet`
--
ALTER TABLE `user_par_projet`
 ADD PRIMARY KEY (`id_user_upp`,`id_projet_upp`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `projet`
--
ALTER TABLE `projet`
MODIFY `id_pro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
