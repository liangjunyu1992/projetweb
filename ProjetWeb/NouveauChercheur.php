<?php session_start();	?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Nouvel Utilisateur</title>
		<script src="javascript/passwordGen.js"></script>
	</head>
	<body>
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menuCon.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1>Création d'un nouvel utilisateur</h1></header>
			
			<?php
			if ($_SESSION['message']=='creer')
			{
				echo '<span>Nouvel utilisateur créer avec succès.</span>';
				$_SESSION['message']='';
			}
			?>
			<div class="contenu">
				<div id="nouveauChercheur">
				
				<?php
				
				if ($_SESSION['type']=='Admin')
				{
				?>
				
				<form method="POST" action="ajouterChercheur.php" name="formu">
						<label for="login">Login</label>
						<input type="text" id="login" name="login" placeholder="Login de l'utilisateur" required>
						<br>
						<label for="mdp">Mot de passe</label>
						<input type="text" id="result" name="result" placeholder="Mot de passe de l'utilisateur">
						<input type="button" value="Générer" onClick="affiche()"><br>
						<br>
						<label for="type">Selectionner un type</label>
						<select id="type" name="type" size="1">
							<option value="Cherc" selected>Chercheur</option>
							<option value="Admin">Administrateur</option>
						</select>
						<br>
						<button name="envoi" value="ok">Créer l'utilisateur</button>
					
				</form>
				
				<?php 
				}
				else 
				{
					echo 'Vous n\'avez pas les droits nécessaire pour créer de nouveau utilisateur.<br> Veuillez vous connecter en tant qu\'Administrateur.';
				}
				?>
					
				</div>
					
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
		
	</body>
</html>
