<?php 	session_start(); 
	require_once("param.inc.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Détail du projet</title>
	</head>
	<body>
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menuCon.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1>Contenu du projet</h1></header>
			
			<?php
			if ($_SESSION['message']=='creer')
			{
				echo '<span>Projet créer avec succès.</span>';
			}
			else if ($_SESSION['message']=='ajouter')
			{
				echo '<span>Nouveau document ajouter avec succès.</span>';
			}
			else if ($_SESSION['message']=='erreur')
			{
				echo '<span>Une erreur s\'est produite lors de l\'ajout du document.</span>';
			}
			
			$_SESSION['message']='';
			
			?>
			<div class="contenu">
			
				<div id="infoProjet">
			
				<?php
				//connexion à la base de donnée

				try
				{
					$bdd = new PDO($dbhost,$dbuser,$dbpassword);
				}
				catch (Exception $e)
				{
					die('Erreur : ' . $e->getMessage());
				}
				
				?>
				
				<?php
				//Récupération du nom et de la description du projet courant dans la base de donnée
				$req = $bdd ->prepare('SELECT * FROM projet WHERE id_pro =:id');
				$req->execute(array(
				    'id' => $_SESSION['idProjetCourant']))or die(print_r($req->errorInfo()));
				$resultat = $req->fetch();
				$_SESSION['nomProjetCourant'] = $resultat['nom_pro'];
				$_SESSION['descProjetCourant'] = $resultat['desc_pro'];
				$req->closeCursor();
				
				?>
				
				<?php
				//L'utilisateur à t'il rejoint le projet ?
				$req = $bdd ->prepare('SELECT * 
							FROM user_par_projet 
							WHERE id_projet_upp=:id_projet AND id_user_upp=:id_user');
				$req->execute(array(
				    'id_projet' => $_SESSION['idProjetCourant'],
				    'id_user' => $_SESSION['id'])) or die(print_r($req->errorInfo()));
				$resultat = $req->fetch();
				
				
				if (!$resultat)
				{
					$aRejointProjet=false; //l'utilisateur n'a pas rejoint le projet
				}
				else
				{
					$aRejointProjet=true; //l'utilisateur a rejoint le projet
				}
				$req->closeCursor();
						
				?>
						
				<?php
				if ($_SESSION['type']!='Cherc')
				{
					echo 'Vous n\'avez pas les droits nécessaire pour créer de nouveau projet.<br> Veuillez vous connecter en tant que Chercheur.';
				}
				else
				{ ?>
					<div align="center" class="liste" id="detailProjet">
				
					<table>
						<tr>
							<th><?php echo '#' . $_SESSION['idProjetCourant']; ?></th>
							<th><?php echo $_SESSION['nomProjetCourant']; ?></td>			
						</tr>
						<tr>
							<td name="description" colspan="2" min-h><?php echo $_SESSION['descProjetCourant']; ?></th>
						</tr>
					</table>
				
					</div>
					
					<?php
					if (!$aRejointProjet)
					{ ?>
						<form method="POST" action="rejoindreProjet.php">
								<button name="idProRejoint" value="<?php echo $_SESSION['idProjetCourant']; ?>">Rejoindre</button>
						</form>
				 <?php	}
				  	else
				  	{ ?>
				  		<br>
				  		
				  		<?php 
				  		$req = $bdd->prepare('SELECT nom_doc, desc_doc, nom_stock_doc 
				  					FROM document 
				  				     	WHERE id_projet_doc = :id_projet');
				  		$req->execute(array(
						    'id_projet' => $_SESSION['idProjetCourant']))or die(print_r($req->errorInfo()));
						$resultat = $req->fetch();
						
						if (!$resultat)
						{
							echo 'Le projet ne compte pour le moment aucun document';
						}
						else
						{?>
						<div align="center" class="liste" id="listeDoc">
							<table>
								<tr>
									<th>Nom Doc</th>
									<th>Description Document</th>
								</tr>
							
							
							<?php
							while ($resultat = $req->fetch())
							{			  		
				  			?>
				  				<tr>
									<td> <?php echo $resultat['nom_doc']; ?> </td>
									<td> <?php echo $resultat['desc_doc']; ?></td>
									<td>
									<form method="post" action="<?php echo 'uploads/'.$resultat['nom_stock_doc']; ?>">
										<button name="consulter" value="">Télécharger</button>
									</form>
									</td>
								</tr>
				  		<?php	}
				  			$req->closeCursor();?>
				  			</table>
				  		</div>
				  		
				  		
				  	<?php	} ?>
				  		<br>
				  		<form method="POST" action="NouveauDoc.php">
								<button name="idProRejoint" value="<?php echo $_SESSION['idProjetCourant']; ?>">Ajouter Document</button>
						</form>
				 <?php	}
				}
				?>
				<form method="POST" action="retourListeProjet.php" name="retour">
					<button name="envoi" value="ok">Quitter</button>
				</form>	
				</div>
					
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
	</body>
</html>
