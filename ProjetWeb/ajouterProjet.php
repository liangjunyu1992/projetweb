<?php 

session_start();
require_once("param.inc.php");

//connexion à la base de donnée

try
{
	$bdd = new PDO($dbhost,$dbuser,$dbpassword);
}
catch (Exception $e)
{
	die('Erreur : ' . $e->getMessage());
}

//Nettoyage des données
$_POST['nomProjet']=htmlentities($_POST['nomProjet'], ENT_QUOTES, "UTF-8");
$_POST['descProjet']=htmlentities($_POST['descProjet'], ENT_QUOTES, "UTF-8");

//vérification de l'inexistence du projet

$req = $bdd->prepare('SELECT nom_pro FROM projet WHERE nom_pro=:nom');
$req->execute(array(
	'nom'=>$_POST['nomProjet'])) or die(print_r($req->errorInfo()));
$resultat=$req->fetch();	
if (!$resultat)//si le projet n'existe pas on le crée.
{
	$req = $bdd ->prepare('INSERT INTO projet(nom_pro, desc_pro) VALUES(:nom,:desc)');
	$req->execute(array(
	    'nom' => $_POST['nomProjet'],
	    'desc' => $_POST['descProjet'])) or die(print_r($req->errorInfo()));
	$req->closeCursor();
	//$_SESSION['projetCourant']=$_POST['nomProjet'];
	
	//on récupère l'id du nouveau projet
	$req = $bdd ->prepare('SELECT id_pro FROM projet WHERE nom_pro =:nom');
	$req->execute(array(
	    'nom' => $_POST['nomProjet']))or die(print_r($req->errorInfo()));
	$resultat = $req->fetch();
	$_SESSION['idProjetCourant']=$resultat['id_pro'];
	$req->closeCursor();
	
	//le créateur du projet rejoint le projet nouvellement créer
	$req = $bdd ->prepare('INSERT INTO user_par_projet(id_user_upp, id_projet_upp) VALUES(:id_user,:id_projet)');
	$req->execute(array(
	    'id_user'=> $_SESSION['id'],
	    'id_projet'=> $_SESSION['idProjetCourant'])) or die(print_r($req->errorInfo()));
	$req->closeCursor();
	
	$_SESSION['message']='creer';
	
	header('Location: DetailProjet.php'); // on redirige vers la page du projet tout juste créé
}
else // on redirige vers la page de création de projet 
{
	header('Location: NouveauProjet.php');
	/*
		Il faudrait pouvoir afficher un message d'erreur sur la page de création, pour prévenir que le projet n'a pas pu être créer.
	*/
}


?>
