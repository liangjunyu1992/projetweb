<?php session_start(); 
	require_once("param.inc.php");?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Administration</title>
	</head>
	<body>
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menuCon.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1>Gestion des chercheurs</h1></header>
			
			<?php
			
			if ($_SESSION['message']=='admin')
			{
				echo '<span>Connexion réussie en tant que Chercheur.</span>';
			}
			else if ($_SESSION['message']=='DNDY')
			{
				echo '<span>Vous ne pouvez pas vous supprimmer vous même.</span>';
			}
			else if ($_SESSION['message']=='confirmesuppression')
			{ ?>
				<span>L'utilisateur a bien été supprimmer</span>
				
		<?php	}
			
			$_SESSION['message']='';
			
			?>
			
			<div class="contenu">

						
			<?php
			if ($_SESSION['type']=='Admin')
			{ ?>

			<div align="center" class="liste">
			
				<table>
					<tr>
						<th>Nom Utilisateur</th>
						<th>Type Utilisateur</th>
					</tr>

			<?php
				
					//connexion à la base de donnée

				try
				{
					$bdd = new PDO($dbhost,$dbuser,$dbpassword);
				}
				catch (Exception $e)
				{
					die('Erreur : ' . $e->getMessage());
				}
				
				//affichage des lignes du tableau avec nom et type des utilisateur
			
				$reponse = $bdd->query('SELECT * FROM user ORDER BY login_user ASC') or die(print_r($req->errorInfo()));
		
				while ($donnees = $reponse->fetch())
					{ ?>
							<tr>
								<td> <?php echo $donnees['login_user']; ?> </td>
								<td> <?php echo $donnees['type_user']; ?> </td>
								<td> 
								<form method="post" action="supprimerChercheur.php">
									<button name="idSupprimmer" value="<?php echo $donnees['id_user']; ?>">Supprimmer</button>
								</form>							
								</td>
							</tr>
				<?php   }
						$reponse->closeCursor();
				
					?>
					
				</table>	
			
			</div>
			
			<?php
			}
			else
			{ 
				echo 'Vous n\'avez pas les droits nécessaire pour afficher cette page.<br> Veuillez vous connecter en tant qu\'administrateur.';
			}
			?>
						
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
		
	</body>
</html>
