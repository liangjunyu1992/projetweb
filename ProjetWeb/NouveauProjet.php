<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Nouveau Projet</title>
	</head>
	<body>
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menuCon.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1>Création d'un nouveau projet</h1></header>
			
			<div class="contenu">
			
				<div id="nouveauProjet">
				
				<?php
				
				if ($_SESSION['type']=='Cherc')
				{
				?>
				
				<form method="POST" action="ajouterProjet.php" name="formu">
						<label for="nomProjet">Nom du projet</label>
						<input type="text" id="nomProjet" name="nomProjet" placeholder="Nom du Projet" required>
						<br>
						<label for="descProjet">Description du projet</label>
						<textarea id="descProjet" name="descProjet" row="10" cols="50" required></textarea>
						<br>
						<button name="envoi" value="ok">Créer le projet</button>
					
				</form>
				
				<?php 
				}
				else 
				{
					echo 'Vous n\'avez pas les droits nécessaire pour créer de nouveau projet.<br> Veuillez vous connecter en tant que Chercheur.';
				}
				?>
					
				</div>
					
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
	</body>
</html>
