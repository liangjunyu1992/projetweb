<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="style.css" />
		<title>Accueil</title>
			<?php require_once("param.inc.php"); //paramêtre de connexion à la base de donnée ?>
	        
	</head>
	<body>
				
		<header>
			<?php include('header.inc.php'); ?>
		</header>
			
		<nav>
			<?php include('menu.inc.php'); ?>
		</nav>
		
		<section class="contenu_bloc">
			<header> <h1 class="STYLE1">Page d'Accueil</h1>
			</header>
			
			<div class="contenu">
			
				<div id="connexion">
			
					<form method="post" action="connexion.php">
						<label for="login">Login</label>
						<input type="text" id="login" name="login" placeholder="Votre identifiant..." required><br>
						<label for="mdp">Mot de passe</label>
						<input type="password" id="mdp" name="mdp" placeholder="********" required><br>
						<button name="envoi" value="ok">Se connecter</button>
					</form>
				
				</div>
			
			</div>
			
		</section>
		
		<footer>
			<?php include('footer.inc.php'); ?>
		</footer>
	</body>
</html>
