<?php

session_start();
require_once("param.inc.php");

//connexion à la base de donnée

try
{
	$bdd = new PDO($dbhost,$dbuser,$dbpassword);
}
catch (Exception $e)
{
	die('Erreur : ' . $e->getMessage());
}

$_SESSION['idProjetCourant']=$_POST['idProRejoint'];

//On vérifie si l'utilisateur n'a pas déjà rejoint le projet.
$req2 = $bdd ->prepare('SELECT * 
			FROM user_par_projet 
			WHERE id_projet_upp=:id_projet AND id_user_upp=:id_user');
$req2->execute(array(
    'id_projet' => $_SESSION['idProjetCourant'],
    'id_user' => $_SESSION['id'])) or die(print_r($req->errorInfo()));
$resultat = $req2->fetch();


if (!$resultat)
{
//rejoindre le projet
$req = $bdd ->prepare('INSERT INTO user_par_projet(id_user_upp, id_projet_upp) VALUES(:id_user,:id_projet)');
$req->execute(array(
    'id_user'=> $_SESSION['id'],
    'id_projet'=> $_SESSION['idProjetCourant'])) or die(print_r($req->errorInfo()));
$req->closeCursor();
$req2->closeCursor();
}

header('Location: DetailProjet.php');


?>
